//
//  EnhanceApp.swift
//  Enhance
//
//  Created by admin on 5/31/23.
//

import SwiftUI

@main
struct EnhanceApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
